package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.model.SortOrder;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/orders")
@Slf4j
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Map<String, Object> fetchOrders(
            @RequestParam(required = false, defaultValue = "0" ) int page,
            @RequestParam(required = false, defaultValue = "10" )int size,
            @RequestParam(required = false, defaultValue = "ASC" )SortOrder order,
            @RequestParam(required = false, defaultValue = "customerName" )String property){
        return this.orderService.fetchOrders(page, size, order, property);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return  this.orderService.fetchOrderByOrderId(orderId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrderByOrderId(id);
    }
}
