package com.classpath.ordermicroservice.model;

public enum SortOrder {
    ASC,
    DESC
}
