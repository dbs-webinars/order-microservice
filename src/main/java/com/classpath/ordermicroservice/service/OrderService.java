package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.model.SortOrder;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.*;

import static com.classpath.ordermicroservice.model.EventType.ORDER_ACCEPTED;
import static java.time.LocalDateTime.now;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;

    public Map<String, Object> fetchOrders(int page, int size, SortOrder order, String property){
        log.info("Fetching the orders ");
        Sort.Direction sortDirection = (order == SortOrder.ASC) ? Sort.Direction.ASC: Sort.Direction.DESC;
        PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        long totalElements = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();

        Set<Order> orders = Set.copyOf(pageResponse.getContent());

        Map<String, Object> resultMap = new LinkedHashMap<>();
        resultMap.put("count", totalElements);
        resultMap.put("pages", totalPages);
        resultMap.put("content", orders);

        return resultMap;
    }

    public Order fetchOrderByOrderId(long orderId){
        log.info("fetching the order by order id :: {}", orderId);
        return this.orderRepository.findById(orderId)
                                    .orElseThrow(() -> new IllegalArgumentException(" Invalid order id passed"));
    }

    @CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
        log.info("saving the order :: {}", order);
        Order savedOrder = this.orderRepository.save(order);
        //ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://inventory-service/api/v1/inventory", null, Integer.class);
        //log.info("Response from inventory service : {}", responseEntity.getBody());
        OrderEvent event = new OrderEvent(order, ORDER_ACCEPTED, now());
        return savedOrder;
    }
    private Order fallback(Exception exception){
        log.error("Exception while communicating with inventory service :: {}", exception.getMessage());
        return Order.builder().build();
    }

    public void deleteOrderByOrderId(long orderId){
        log.info("deleting the order by order id :: {}", orderId);
        this.orderRepository.deleteById(orderId);
    }
}
